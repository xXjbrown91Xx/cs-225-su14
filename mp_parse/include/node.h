/**
 * @file node.h
 * @author Jorden Brown
 * @date Summer 2014
 */

#ifndef CS225_NODE_H_
#define CS225_NODE_H_

namespace cs225
{
class node
{
	public:
		virtual double value() const = 0;
		virtual ~node() = default;
};
}
#endif
