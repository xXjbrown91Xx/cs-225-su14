/**
 * @file divide.h
 * @author Jorden Brown
 * date Summer 2014
 */

#ifndef CS225_DIVIDE_H_
#define CS225_DIVIDE_H_

#include "binary_op.h"

namespace cs225
{
class times : public binary_op
{
	public:
		using binary_op::binary_op;
		double combine(double left, double right) const override;
	private:
};
}
#endif
