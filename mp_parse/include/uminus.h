/**
 * @file uminus.h
 * @author Jorden Brown
 * @date Summer 2014
 */

#ifndef CS225_UMINUS_H_
#define CS225_UMINUS_H_

#include "node.h"
#include "unary_op.h"

namespace cs225
{
class uminus : public unary_op
{
	public:
		using unary_op::unary_op;
		double compute(double arg) const override;
	private:
};
}
#endif
