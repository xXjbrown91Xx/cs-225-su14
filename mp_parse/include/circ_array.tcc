/**
 * @file circ_array.h
 * @author Chase Geigle
 * @date Summer 2014
 */

#include <stdexcept>
#include "circ_array.h"

namespace cs225
{

template <class T>
circ_array<T>::circ_array()
{
	arr_ = nullptr;
	size_ = 0;
	capacity = 0;
}
template <class T>
circ_array<T>::circ_array(uint64_t size)
{
	size_ = size;
	capacity = size;
	arr_.reset(new T[size]);
}

template <class T>
circ_array<T>::circ_array(const circ_array& other)
{
	/* Copy over the size */
	size_ = other.size_;
	capacity = other.capacity;
	/* Create a new array of others size, and fill in the copy */
	arr_.reset(new T[size_]);
	for(uint64_t i = 0; i<size_; i++)
	{
		//arr_[i].get() = other.arr_[i].get();
		arr_[i] = other.arr_[i];
	}
}

template <class T>
circ_array<T>::circ_array(circ_array&& other)
{
	/* Set up the current instance to be blank */
	size_ = 0;
	capacity = 0;
	arr_.reset(nullptr);
	/* Swap current with other */
	swap(other);
}

template <class T>
circ_array<T>& circ_array<T>::operator=(circ_array rhs)
{
	if(this != &rhs)
	{
		/* Release any resources that *this owns */
		arr_.reset(nullptr);
		/* Swap resources with other */
		swap(rhs);
		/* Return new instance */
		return *this;
	}
}

template <class T>
circ_array<T>::~circ_array() = default;

template <class T>
void circ_array<T>::swap(circ_array& other)
{
	std::swap(size_, other.size_);
	std::swap(capacity, other.capacity);
	arr_.swap(other.arr_);
}

template <class T>
const T& circ_array<T>::operator[](uint64_t idx) const
{
	if(arr_ != nullptr)
	{
		return arr_[idx];
	}
	throw;
}

template <class T>
T& circ_array<T>::operator[](uint64_t idx)
{
	if(arr_ != nullptr)
	{
		return arr_[idx];
	}
	throw;
}

template <class T>
const T& circ_array<T>::at(uint64_t idx) const
{
	if(idx >= size_)
	{
		throw std::out_of_range{":invalid index"};
	}
	return arr_[idx];
}

template <class T>
T& circ_array<T>::at(uint64_t idx)
{
	if(idx >= size_)
	{
		throw std::out_of_range{":invalid index"};
	}
	return arr_[idx];
}

template <class T>
void circ_array<T>::push_front(const T& elem)
{
	if(arr_ == nullptr)
	{
		return;
	}
	/* If there are no elements in the array, simply assign */
	if(capacity == 0)
	{
		arr_[0] = elem;
		capacity++;
		return;
	}
	/* Resize the array when at maximum capacity */
	if(capacity == size_)
	{
		resize();
	}
	/* Increment the index of each node */
	for(uint64_t i = (size_/2); i>0; i--)
	{
		arr_[i] = arr_[i-1];
	}
	/* Push in element, increment capacity */
	arr_[0] = std::move(elem);
	capacity++;
	return;
}

template <class T>
void circ_array<T>::push_front(T&& elem)
{
	if(arr_ == nullptr)
	{
		return;
	}
	/* If there are no elements in the array, simply assign */
	if(capacity == 0)
	{
		arr_[0] = elem;
		capacity++;
		return;
	}
	/* Resize the array when at maximum capacity */
	if(capacity == size_)
	{
		resize();
	}
	/* Increment the index of each node */
	for(uint64_t i = (size_/2); i>0; i--)
	{
		arr_[i] = arr_[i-1];
	}
	/* Push in element, increment capacity */
	arr_[0] = std::move(elem);
	capacity++;
	return;
}

template <class T>
void circ_array<T>::push_back(const T& elem)
{
	/* If array is empty, simplty assign */
	if(capacity == 0)
	{
		arr_[0] = elem; // Potential issue
		capacity++;
		return;
	}
	/* If capacity is max, resize */
	if(capacity == size_)
	{
		resize();
	}
	/* Push element, update capacity */
	arr_[capacity] = elem;
	capacity++;
}

template <class T>
void circ_array<T>::push_back(T&& elem)
{
	/* If array is empty, simply assign */
	if(capacity == 0)
	{
		arr_[0] = std::move(elem);
		capacity++;
		return;
	}
	/* If capacity is max, resize */
	if(capacity == size_)
	{
		resize();
	}
	/* Push element, update capacity */
	arr_[capacity] = std::move(elem);
	capacity++;
}

template <class T>
void circ_array<T>::pop_front()
{
	/* Do nothing if empty */
	if(arr_ == nullptr && size_ == 0 && capacity == 0)
	{
		return;
	}
	/* Decrement capacity if one element */
	if(capacity == 1)
	{
		capacity--;
	}
	/* Create new array */
	std::unique_ptr<T[]> temp;
	temp.reset(new T[size_]);
	/* Copy over all elements except first */
	for(uint64_t i = 1; i<size_; i++)
	{
		temp[i-1] = arr_[i];
	}
	/* Update */
	arr_.reset(nullptr);
	arr_ = std::move(temp);
	capacity--;
}

template <class T>
void circ_array<T>::pop_back()
{
	/* Do nothing if empty */
	if(arr_ == nullptr && size_ == 0 && capacity == 0)
	{
		return;
	}
	/* Decrement capacity if one element */
	if(capacity == 1)
	{
		capacity--;
	}
	/* Create new array */
	std::unique_ptr<T[]> temp;
	temp.reset(new T[size_]);
	/* Copy over all elements except last */
	for(uint64_t i = 0; i<size_-1; i++)
	{
		temp[i] = std::move(arr_[i]);
	}
	/* Update */
	arr_.reset(nullptr);
	arr_ = std::move(temp);
	capacity--;
}

template <class T>
void circ_array<T>::resize()
{
	size_ = 2*size_;
	/* Create new array */
	std::unique_ptr<T[]> temp;
	temp.reset(new T[size_]);
	/* Copy over all elements */
	for(uint64_t i = 0; i<size_/2; i++)
	{
		temp[i] = std::move(arr_[i]);
	}
	/* Update */
	arr_.reset(nullptr);
	arr_ = std::move(temp);
}

template <class T>
void circ_array<T>::erase(uint64_t idx)
{
	/* Shift all necessary elements down */
	for(uint64_t i = idx + 1; i<size_; i++)
	{
		arr_[i-1] = arr_[i];
	}
	/* Decrement size */
	capacity--;
}

template <class T>
uint64_t circ_array<T>::size() const
{
	return capacity;
}

template <class T>
bool circ_array<T>::empty() const
{
	if(arr_ == nullptr && size_ == 0 && capacity == 0)
	{
		return true;
	}
	return false;
}
}
