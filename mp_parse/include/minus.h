/**
 * @file minus.h
 * @author Jorden Brown
 * @date Summer 2014
 */

#ifndef CS225_MINUS_H_
#define CS225_MINUS_H_

#include "binary_op.h"

namespace cs225
{
class minus : public binary_op
{
	public:
		using binary_op::binary_op;
		double combine(double left, double right) const override;
	private:
};
}
#endif
