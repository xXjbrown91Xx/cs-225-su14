/**
 * @file terminal.h
 * @author Jorden Brown
 * @date Summer 2014
 */

#ifndef CS225_TERMINAL_H_
#define CS225_TERMINAL_H_

#include "node.h"
#include <memory>

namespace cs225
{
class terminal : public node
{
	public:
		terminal(double value);
		double value() const override;
	private:
		double value_;
};
std::unique_ptr<terminal> make_term(double value);
}
#endif
