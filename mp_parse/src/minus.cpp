/**
 * @file minus.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "minus.h"
namespace cs225
{
double minus::combine(double left, double right) const
{
	return left - right;
}
}
