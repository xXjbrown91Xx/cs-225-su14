/**
 * @file unary_op.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "unary_op.h"
#include "uplus.h"
#include "uminus.h"
#include <string>
#include <sstream>
#include <iostream>
#include <utility>
#include <algorithm>

namespace cs225
{
unary_op::unary_op(std::unique_ptr<node> arg)
{
	arg_ = std::move(arg);
}

double unary_op::value() const
{
	return compute(arg_->value());
}

std::unique_ptr<node> make_unary_op(const std::string &op, std::unique_ptr<node> arg)
{
	if(op.compare("#")==0)
	{
		std::unique_ptr<unary_op> base = std::make_unique<uplus>(std::move(arg));
		std::unique_ptr<node> ptr = std::move(base);
		return ptr;
	}
	if(op.compare("~") == 0)
	{
		std::unique_ptr<unary_op> base = std::make_unique<uminus>(std::move(arg));
		std::unique_ptr<node> ptr = std::move(base);
		return ptr;
	}
}
}
