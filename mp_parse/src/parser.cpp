/**
 * @file parser.cpp
 * @author Chase Geigle
 * @date Summer 2014
 */

#include <cassert>
#include "parser.h"
#include "operation.h"
#include "number.h"
#include "terminal.h"
#include "binary_op.h"
#include "unary_op.h"

namespace cs225
{

/**
 * A "private class" for helping us parse expressions. Maintains all of the
 * state necessary to perform Dijkstra's shunting-yard algorithm.
 *
 * @see https://en.wikipedia.org/wiki/Shunting-yard_algorithm
 */
class parser
{
  public:
    /**
     * @param tokens The tokens to parse
     * @return the root of the parse tree for that expression
     */
    std::unique_ptr<node> parse(queue<std::unique_ptr<token>>& tokens);

  private:
    // add lots of other helper methods here!

    /**
     * The stack to hold the operator tokens encountered. This is the "side
     * rail" in the shunting yard algorithm.
     */
    stack<std::unique_ptr<token>> ops_;

    /**
     * What is this for? It is a mystery (and a hint)!
     */
    stack<std::unique_ptr<node>> nodes_;
};

std::unique_ptr<node> parse(queue<std::unique_ptr<token>>& tokens)
{
    parser p;
    return p.parse(tokens);
}

std::unique_ptr<node> parser::parse(queue<std::unique_ptr<token>>& tokens)
{
	/* While there are tokens to be read */
    while (!tokens.empty())
    {
		/* Read a token, then pop */
        auto& tok = tokens.front();
		tokens.pop();
        /* If the token is a number, add to the output queue */
		if(tok->type() == token_type::NUMBER)
		{
			//tokens.push(tok.as_number());
			nodes_.push(tok.as_number());
		}
		/* If the token is a function token, push it onto the operator stack */
		if(tok->type() == token_type::OPERATION)
		{
			ops_.push(tok.as_operation());
		}
		/* If the token is a function token, pop operators off ops_ onto nodes_ */
		if(tok->type() == token_type::TEXT)
		{
			/* While there are operators to be popped */
			while(!ops_.empty())
			{
				auto& temp = ops_.top();
				/* Check if the top of the ops_ stack is '(' */
				if(temp != "(")
				{
					/* Pop from the ops_ and add to tokens */
					ops_.pop();
					nodes_.push(temp);
				}
				else
				{
					break;
				}
			}
			/* If we make it out of the loop and temp != '(' there is an error */
			if(temp != "(")
			{
				throw std::runtime_error{":unbalanced parens"};
			}
		}
		/* If the token is an operator, pop other operators and push onto output queue */
		if(tok->type() == token_type::OPERATION)
		{
			/* While there is an operator token at the top of the stack */
			
		
	
    }

    /// @todo clean up remaining operators
    /// @todo return the root of your tree
}
}
