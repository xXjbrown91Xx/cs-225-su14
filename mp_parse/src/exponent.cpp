/**
 * @file divide.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "exponent.h"
#include <math.h>

namespace cs225
{
double exponent::combine(double left, double right) const
{
	return pow(left, right);
}
}
