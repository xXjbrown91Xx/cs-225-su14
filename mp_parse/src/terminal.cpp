/**
 * @file terminal.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "terminal.h"
#include <utility>
#include <algorithm>

namespace cs225
{
terminal::terminal(double value)
{
	value_ = value;
}

double terminal::value() const
{
	return value_;
}

std::unique_ptr<terminal> make_term(double value)
{
	/* Create a unique terminal pointer */
	std::unique_ptr<terminal> term = std::make_unique<terminal>(value);  //Might be wrong
	/* Return */
	return term;
}
}
