/**
 * @file divide.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "divide.h"
namespace cs225
{
double divide::combine(double left, double right) const
{
	return left / right;
}
}
