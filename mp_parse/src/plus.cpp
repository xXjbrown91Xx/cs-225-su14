/**
 * @file minus.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "plus.h"

namespace cs225
{

double plus::combine(double left, double right) const
{
	return left + right;
}
}
