/**
 * @file uminus.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "uminus.h"
namespace cs225
{
double uminus::compute(double val) const
{
	return (val*-1);
}
}
