/**
 * @file divide.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "times.h"

namespace cs225
{
double times::combine(double left, double right) const
{
	return left * right;
}
}
