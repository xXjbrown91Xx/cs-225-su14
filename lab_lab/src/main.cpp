/**
 * @file main.cpp
 * CS 225 lab_lab
 */

#include "epng.h"  // to use the PNG class

using namespace epng;

void removeColor()
{
    /* Open image */
    png image("in.png");

    /* loop through image change green channel to zero */
    for(size_t i=0;i<(image.width());i++)
    {
        for(size_t j=0;j<(image.height());j++)
	{
	    image(i,j)->green = 0;
	}
    }

    /* Save the image */
    image.save("removed.png");
}

int main()
{
    removeColor();
    return 0;
}
