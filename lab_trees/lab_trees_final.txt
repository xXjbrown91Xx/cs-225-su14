Monad Autograder
Testing lab_trees at 2014-07-10 23:18:04

"Recursion is magic. " ~ Prof. Jeff Erickson | "Recursion is NOT magic." ~ Prof. Craig Zilles 

Setting up test environment...
Compiling...
================================================================


Running tests...
================================================================
test_isOrdered  . [25 pts] - FAILED: unit_tests.cpp:31: Assertion (tree.isOrdered() == false) failed
test_mirror . . . [25 pts] - passed
test_printPaths . [25 pts] - FAILED: Incorrect Terminal Output
test_sumDistances [25 pts] - FAILED: unit_tests.cpp:142: [tree.sumDistances() => 0] != [19ul => 19]


Total score...
================================================================
TOTAL SCORE: 25



Detailed test output...
================================================================
----------------------------------------------------------------
test_isOrdered [25 points]
Result: FAILED: unit_tests.cpp:31: Assertion (tree.isOrdered() == false) failed
Took 0ms (1000ms timeout)
Output:
----------------------------------------------------------------
                        ______ 6 ______                          
                 ______/               \______                   
            __ 5 __                            0 __              
         __/       \__                             \__           
       9               8                               3         
                     /                               /   \       
                   2                               7       1     
                                                    \            
                                                     4           

----------------------------------------------------------------
test_printPaths [25 points]
Result: FAILED: Incorrect Terminal Output
Took 0ms (1000ms timeout)
Output:
----------------------------------------------------------------

----------------------------------------------------------------
test_sumDistances [25 points]
Result: FAILED: unit_tests.cpp:142: [tree.sumDistances() => 0] != [19ul => 19]
Took 0ms (1000ms timeout)
Output:
----------------------------------------------------------------
            __ 5 __              
         __/       \__           
       3               7         
     /   \           /   \       
   1       4       6       8     
  / \                       \    
 0   2                       9   


----------------------------------------------------------------
Total score...
================================================================
TOTAL SCORE: 25

