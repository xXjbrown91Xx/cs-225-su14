/**
 * @file binary_tree.tcc
 * Definitions of the binary tree functions you'll be writing for this lab.
 * You'll need to modify this file.
 */

template <class T>
int binary_tree<T>::height() const
{
    // Call recursive helper function on root
    return height(root_.get());
}

template <class T>
int binary_tree<T>::height(const node* subRoot) const
{
    // Base case
    if (!subRoot)
        return -1;

    // Recursive definition
    auto left = height(subRoot->left.get());
    auto right = height(subRoot->right.get());
    return std::max(left, right) + 1;
}

template <class T>
void binary_tree<T>::printLeftToRight() const
{
    // Call recursive helper function on the root
    printLeftToRight(root_.get());

    // Finish the line
    std::cout << std::endl;
}

template <class T>
void binary_tree<T>::printLeftToRight(const node* subRoot) const
{
    // Base case - null node
    if (!subRoot)
        return;

    // Print left subtree
    printLeftToRight(subRoot->left.get());

    // Print this node
    std::cout << subRoot->elem << ' ';

    // Print right subtree
    printLeftToRight(subRoot->right.get());
}

template <class T>
void binary_tree<T>::mirror()
{
	mirror_helper(root_.get());
}

template <class T>
void binary_tree<T>::mirror_helper(binary_tree<T>::node * head)
{
	if(head == nullptr)
	{
		return;
	}
	mirror_helper(head->left.get());
	mirror_helper(head->right.get());
	std::unique_ptr<binary_tree<T>::node> temp = std::move(head->left);
	head->left = std::move(head->right);
	head->right = std::move(temp);
}

template <class T>
bool binary_tree<T>::isOrdered() const
{
	//return isOrdered_helper(root_.get());
	return true;	
}
/*
template <class T>
bool binary_tree<T>::isOrdered_helper(binary_tree<T>::node * root)
{	
	binary_tree<T>::node * previous = nullptr;

	if(root == nullptr)
	{
		return true;
	}
	if(!isOrdered_helper(root->left.get()))
	{
		return false;
	}
	if(previous != nullptr && root->elem <= prev->elem)
	{
		return false;
	}
	previous = root.get();
	return isOrdered_helper(root->right.get());
}
*/
template <class T>
void binary_tree<T>::printPaths() const
{
    /// @todo your code here
}

template <class T>
uint64_t binary_tree<T>::sumDistances() const
{
    /// @todo your code here
    return 0;
}
