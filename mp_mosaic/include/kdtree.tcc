/**
 * @file kdtree.tcc
 * Implementation of kd_tree class.
 */

#include "kdtree.h"
#include "math.h"

template <int Dim>
bool kd_tree<Dim>::smaller_in_dimension(const point<Dim>& first,
                                       const point<Dim>& second,
                                       int curDim) const
{
	if(first[curDim] == second[curDim])
	{
		return (first < second);
	}
	return (first[curDim] < second[curDim]);
}

template <int Dim>
bool kd_tree<Dim>::should_replace(const point<Dim>& target,
                                 const point<Dim>& current_best,
                                 const point<Dim>& potential) const
{
	uint64_t pdist = distance(target, potential);
	uint64_t best_dist = distance(target, current_best);

	if(pdist == best_dist)
	{
		return (potential < current_best);
	}
	return(pdist < best_dist);
}

template <int Dim>
uint64_t kd_tree<Dim>::distance(const point<Dim>& target, const point<Dim>& potential) const
{
	uint64_t distance = 0;

	for(int i = 0; i < Dim; i++)
	{
		distance += pow((target[i] - potential[i]), 2);
	}
	return distance;
}

template <int Dim>
kd_tree<Dim>::kd_tree(const std::vector<point<Dim>>& newpoints)
{
	// Update points in tree if necessary
	if(!newpoints.empty())
	{
		points.resize(newpoints.size());
		for(uint64_t i = 0; i<newpoints.size(); i++)
		{
			points[i] = newpoints[i];
		}	
		// Find the median of first set of points
		build_kd(points,0,0,(points.size())-1);
	}
}

template <int Dim>
void kd_tree<Dim>::build_kd(std::vector<point<Dim>>& elements, int dim, int low, int high)
{
	int mid = (low+high)/2;
	// Found a leaf node
	if(low >= high)
	{
		return;
	}
	// Find our median, and put it where it needs to be
	find_median(elements, dim, low, high, mid);
	// Recursively create the left and right subtrees
	build_kd(elements, (dim+1)%Dim, low, mid-1);
	build_kd(elements, (dim+1)%Dim, mid+1, high);
}

template <int Dim>
void kd_tree<Dim>::find_median(std::vector<point<Dim>>& elements, int dim, int low, int high, int mid)
{
	// Found a leaf node
	if(low >= high)
	{
		return;
	}
	
	int mid_idx = (low+high)/2;
	int part = partition(elements, low, high, mid_idx, dim);

	// Found a root node
	if(mid == part)
	{
		return;
	}
	// Go right
	else if(part < mid)
	{
		return find_median(elements, dim, part+1, high, mid_idx);
	}
	// Go left
	else
	{
		return find_median(elements, dim, low, part-1, mid_idx);
	}	
}

template <int Dim>
int kd_tree<Dim>::partition(std::vector<point<Dim>>& elements, int low, int high, int mid, int dim)
{
	// Move the pivot to the end of the array
	swap(elements[mid], elements[high]);
	// Store the index of the left most point
	int stored_index = low;
	// Iterate over list putting elements in correct orientation relative to pivot
	for(int i = low; i<high; i++)
	{
		if(smaller_in_dimension(elements[i], elements[high], dim))
		{
			swap(elements[stored_index], elements[i]);
			stored_index++;
		}
	}
	// Move pivot into final positon
	swap(elements[high], elements[stored_index]);
	// Return the partition location
	return stored_index;
}

template <int Dim>
point<Dim> kd_tree<Dim>::find_nearest_neighbor(const point<Dim>& query) const
{
	return nearest_helper(query, 0, points.size()-1, 0);
}

template <int Dim>
point<Dim> kd_tree<Dim>::nearest_helper(const point<Dim>& query, int low, int high, int dim) const
{
	int mid = (low+high)/2;
	point<Dim> curr = points[mid];
	point<Dim> curr_best;
	point<Dim> poss;

	if(low >= high)
	{
		return points[low];
	}

	// Compare dimensions to determine direction
	if(smaller_in_dimension(query, curr, dim))
	{
		curr_best = nearest_helper(query, low, mid-1, (dim+1)%Dim);
	}
	else
	{
		curr_best = nearest_helper(query, mid+1, high, (dim+1)%Dim);
	}

	// Check if current node is better than curr best
	if(should_replace(query,curr_best,curr))
	{
		curr_best = curr;
	}
	
	// Check if any closer nodes exist in other side of splitting plane
	if(pow((query[dim] - curr[dim]),2) < distance(query, curr_best))
	{
		if(smaller_in_dimension(query, curr, dim))
		{
			poss = nearest_helper(query, mid+1, high, (dim+1)%Dim);
		}
		else
		{
			poss = nearest_helper(query, low, mid-1, (dim+1)%Dim);
		}
		if(should_replace(query,curr_best,poss))
		{
			curr_best = poss;
		}
	}

	return curr_best;
}

template <int Dim>
void kd_tree<Dim>::swap(point<Dim>& a, point<Dim>& b)
{
	point<Dim> temp = a;
	a = b;
	b = temp;
}
