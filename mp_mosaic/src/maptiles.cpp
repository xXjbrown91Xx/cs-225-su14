/**
 * @file maptiles.cpp
 * Code for the maptiles function.
 */

#include <iostream>
#include <map>

#include "maptiles.h"

mosaic_canvas map_tiles(const source_image& source,
                        const std::vector<tile_image>& tiles)
{
	// Throw in case of bad input
	if(!source.rows() || !source.columns() || tiles.empty())
	{
		throw std::out_of_range{":invalid input"};
	}
	// Create a new mosaic_canvas
	mosaic_canvas * canvas = new mosaic_canvas(source.rows(), source.columns());
	// Create a 3-dim vector of points and map adt
	std::vector<point<3>> points;
	std::map<epng::rgba_pixel, tile_image> cells; 

	// Convert tiles to points
	for(uint64_t i = 0; i<tiles.size(); i++)
	{
		// Create an rgba_pixel with the average color of the current tile
		epng::rgba_pixel avg = tiles[i].average_color();
		// Create a 3 dimensional point using the RGB values as input
		point<3> newpoint(avg.red, avg.green, avg.blue);
		// Create a new map element using the 3D point 
		std::pair<epng::rgba_pixel, tile_image> map_tile(avg, tiles[i]);
		// Add the points to our vector and map
		points.push_back(newpoint);
		cells.insert(map_tile);
	}
	// Create our kdtree using our points vector
	kd_tree<3> img(points);

	//Loop through entirety of image, checking to see which tile_image best fits the source
	for(int i = 0; i<source.rows(); i++)
	{
		for(int j = 0; j<source.columns(); j++)
		{
			// Get the current region
			epng::rgba_pixel reg_col = source.region_color(i,j);
			// Create a point to use for find_nearest_neighbor
			point<3> query(reg_col.red, reg_col.green, reg_col.blue);
			// Find the tile that is closest to our current region in color
			point<3> closest = img.find_nearest_neighbor(query);
			// Create an RGBAPixel from closest
			epng::rgba_pixel close(closest[0], closest[1], closest[2]);
			// Create a mosaic tile for the image and place it in the mosaic
			std::map<epng::rgba_pixel, tile_image>::iterator iter = cells.find(close);
			tile_image elem = iter->second;
			canvas->set_tile(i,j,elem);
		}
	}
	return (*canvas);	
}
