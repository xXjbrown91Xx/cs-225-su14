/**
 * @file quadtree.cpp
 * quadtree class implementation.
 * @date Spring 2008
 * @date Modified Summer 2014
 */

#include "quadtree.h"
#include "math.h"

#define MAX 2000000

namespace cs225
{
quadtree::quadtree()
{
	root_.reset(nullptr);
	res_ = 0;
}

quadtree::quadtree(const epng::png & source, uint64_t resolution)
{
	res_ = resolution;
	build_tree(source, resolution);
}

quadtree::quadtree(const quadtree &other)
{
	root_.reset(nullptr);  // Possible issue here
	quadcopy(root_, other.root_);
	res_ = other.res_;
}

quadtree::quadtree(quadtree &&other)
{
	res_ = 0;
	root_.reset(nullptr);
	swap(other);
}

quadtree& quadtree::operator=(quadtree other)
{
	if(this != &other)
	{
		root_.reset(nullptr);
		quadcopy(root_, other.root_);
	}
	return *this;
}

void quadtree::swap(quadtree &other)
{
	root_.swap(other.root_);  // May be an issue
	std::swap(res_, other.res_);
}

void quadtree::build_tree(const epng::png & source, uint64_t resolution)
{
	res_ = resolution;
	buildTree(source, resolution,0,0,root_);
}


const epng::rgba_pixel & quadtree::operator()(uint64_t x, uint64_t y) const
{
	if(root_.get() == nullptr || x>=res_ || y>=res_)
	{
		throw std::out_of_range{":invalid index"};
	}
	
	return getPixel(x,y,res_,root_.get());
}

const epng::rgba_pixel & quadtree::getPixel(uint64_t x, uint64_t y, uint64_t resolution, node * root) const
{
	uint64_t res = resolution/2;
	if(resolution==1 || (root->northwest.get() == nullptr && root->northeast.get() == nullptr && root->southwest.get() == nullptr && root->southeast.get() == nullptr))
	{
		return root->element; // May have to use root.get()
	}
	else
	{
		if(x<res && y<res)
		{
			return getPixel(x,y,(resolution/2),root->northwest.get());
		}
		if(x>=res && y<res)
		{
			return getPixel((x-(resolution/2)),y,(resolution/2),root->northeast.get());
		}
		if(x<res && y>=res)
		{
			return getPixel(x, (y-(resolution/2)), (resolution/2), root->southwest.get());
		}
		if(x>=res && y>=res)
		{
			return getPixel((x-(resolution/2)), (y-(resolution/2)), (resolution/2), root->southeast.get());
		}
		else
		{
			return root->element;
		}
	}
}

epng::png quadtree::decompress() const
{
	if(root_.get() == nullptr)
	{
		return epng::png(res_, res_);
	}

	epng::png img(res_, res_);

	for(uint64_t i = 0; i<res_; i++)
	{
		for(uint64_t j = 0; j<res_; j++)
		{
			*img(i,j) = getPixel(i,j,res_,root_.get());
		}
	}
	return img;
}
void quadtree::rotate_clockwise()
{
	if(root_.get() == nullptr)
	{
		return;
	}
	rotate_clockwise(root_.get());
}

void quadtree::rotate_clockwise(node * root)
{
	if(root->northwest.get() == nullptr)
	{
		return;
	}

	node * nwtemp = root->northwest.release();
	node * netemp = root->northeast.release();
	node * swtemp = root->southwest.release();
	node * setemp = root->southeast.release();

	root->northwest.reset(swtemp);
	root->northeast.reset(nwtemp);
	root->southwest.reset(setemp);
	root->southeast.reset(netemp);

	rotate_clockwise(root->northwest.get());
	rotate_clockwise(root->northeast.get());
	rotate_clockwise(root->southwest.get());
	rotate_clockwise(root->southeast.get());
}

void quadtree::prune(uint32_t tolerance)
{
	if(root_.get() == nullptr)
	{
		return;
	}
	prune(tolerance, root_.get());
}

void quadtree::prune(uint32_t tolerance, node * root)
{
	if(root == nullptr)
	{
		return;
	}

	if(pruner(tolerance,root,root))
	{
		root->northwest.reset(nullptr);
		root->northeast.reset(nullptr);
		root->southwest.reset(nullptr);
		root->southeast.reset(nullptr);
	}
	else
	{
		prune(tolerance, root->northwest.get());
		prune(tolerance, root->northeast.get());
		prune(tolerance, root->southwest.get());
		prune(tolerance, root->southeast.get());
	}
}

bool quadtree::pruner(uint32_t tolerance, node * curr, node * root)
{
	if(root == nullptr)
	{
		return false;
	}
	
	if(!has_children(root))
	{
		return ((pow((root->element.red - curr->element.red), 2) + pow((root->element.green - curr->element.green),2) + pow((root->element.blue - curr->element.blue),2)) <= tolerance);
	}

	return pruner(tolerance,curr,root->northwest.get()) && pruner(tolerance,curr,root->northeast.get()) && pruner(tolerance,curr,root->southwest.get()) && pruner(tolerance,curr,root->southeast.get());
}
uint64_t quadtree::pruned_size(uint32_t tolerance) const
{
	if(root_.get() == nullptr)
	{
		return 0;
	}
	return pruned_size(tolerance, root_.get());
}

uint64_t quadtree::pruned_size(uint32_t tolerance,  node * root) const
{
	if(root == nullptr)
	{
		return 0;
	}
	if(sizehelper(tolerance, root, root))
	{
		return 1;
	}
	else
	{
		return pruned_size(tolerance, root->northwest.get()) + pruned_size(tolerance, root->northeast.get()) + pruned_size(tolerance, root->southwest.get()) + pruned_size(tolerance, root->southeast.get());
	}
}

bool quadtree::sizehelper(uint32_t tolerance, node * curr, node * root) const
{
	if(!has_children(root))
	{
		return ((pow((root->element.red - curr->element.red), 2) + pow((root->element.green - curr->element.green),2) + pow((root->element.blue - curr->element.blue),2)) <= tolerance);
	}
	return sizehelper(tolerance,curr,root->northwest.get()) && sizehelper(tolerance,curr,root->northeast.get()) && sizehelper(tolerance,curr,root->southwest.get()) && sizehelper(tolerance,curr,root->southeast.get());
}	
uint32_t quadtree::ideal_prune(uint64_t num_leaves) const
{
	if(root_.get() == nullptr)
	{
		return 0;
	}
	return ideal_prune(num_leaves, 0, MAX);
}

uint32_t quadtree::ideal_prune(uint64_t num_leaves, uint32_t min, uint32_t max) const
{
	auto middle = (max+min)/2;

	if(min >= max)
	{
		return min;
	}
	if(pruned_size(middle) > num_leaves)
	{
		return ideal_prune(num_leaves, middle+1, max);
	}
	else
	{
		return ideal_prune(num_leaves, min, middle);
	}
}

bool quadtree::has_children(node * root)
{
	if(root == nullptr)
	{
		return false;
	}
	if(root->northwest.get() == nullptr && root->southwest.get() == nullptr && root->southeast.get() == nullptr && root->northeast.get() == nullptr)
	{
		return false;
	}
	return true;
}

bool quadtree::has_children(node * root) const
{
	if(root == nullptr)
	{
		return false;
	}
	if(root->northwest.get() == nullptr && root->southwest.get() == nullptr && root->southeast.get() == nullptr && root->northeast.get() == nullptr)
	{
		return false;
	}
	return true;
}

void quadtree::quadcopy(std::unique_ptr<node> &root1, std::unique_ptr<node> const &root2)
{
	if(!root2.get())
	{
		return;
	}

	root1 = std::make_unique<node>(std::move(root2->element));

	// Recursively create the child nodes 
	quadcopy(root1->northwest, root2->northwest);
	quadcopy(root1->northeast, root2->northeast);
	quadcopy(root1->southwest, root2->southwest);
	quadcopy(root1->southeast, root2->southeast);
}

void quadtree::buildTree(const epng::png &source, uint64_t resolution, uint64_t x, uint64_t y, std::unique_ptr<node> &root)
{
	root = std::make_unique<node>();

	// Base case
	if(resolution == 1)
	{
		root->element = *source(x,y);
		return;
	}

	// Recursively build trees
	buildTree(source, resolution/2, x, y, root->northwest);
	buildTree(source, resolution/2, x+(resolution/2), y, root->northeast);
	buildTree(source, resolution/2, x, y+(resolution/2), root->southwest);
	buildTree(source, resolution/2, x+(resolution/2), y+(resolution/2), root->southeast);
	
	// Update element
	root->element.red = ((root->northwest->element.red + root->northeast->element.red + root->southwest->element.red + root->southeast->element.red)/4);	
	root->element.green = ((root->northwest->element.green + root->northeast->element.green + root->southwest->element.green + root->southeast->element.green)/4);	
	root->element.blue = ((root->northwest->element.blue + root->northeast->element.blue + root->southwest->element.blue + root->southeast->element.blue)/4);	
}
}
