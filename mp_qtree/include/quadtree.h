/**
 * @file quadtree.h
 * quadtree class definition.
 * @date Spring 2008
 * @date Modified Summer 2014
 */

#ifndef QUADTREE_H_
#define QUADTREE_H_

#include <iostream>
#include "epng.h"

namespace cs225
{

/**
 * A tree structure that is used to compress epng::png images.
 */
class quadtree
{
  public:
	quadtree();
	quadtree(const epng::png &source, uint64_t resolution);
	quadtree(const quadtree &other);
	quadtree(quadtree &&other);
	~quadtree()=default;
	quadtree& operator=(quadtree other);
	void swap (quadtree &other);
	void build_tree(const epng::png &source, uint64_t resolution);
	const epng::rgba_pixel & operator()(uint64_t x, uint64_t y) const;
	epng::png decompress() const;
	void rotate_clockwise();
	void prune(uint32_t tolerance);
	uint64_t pruned_size(uint32_t tolerance) const;
	uint32_t ideal_prune(uint64_t num_leaves) const;
	//void print(std::ostream &out=std::cout) const;
	//bool operator==(const quadtree &other) const;
	
  private:
    /**
     * A simple class representing a single node of a quadtree.
     * You may want to add to this class; in particular, it could
     * probably use a constructor or two...
     */
    class node
    {
      public:
        std::unique_ptr<node> northwest;
        std::unique_ptr<node> northeast;
        std::unique_ptr<node> southwest;
        std::unique_ptr<node> southeast;

        epng::rgba_pixel element; // the pixel stored as this node's "data"

		/* No arg constructor */
		node() : northwest{nullptr}, northeast{nullptr}, southwest{nullptr}, southeast{nullptr}
		{
			element = epng::rgba_pixel::rgba_pixel();
		} 
		
		/* One arg constructor */
		node(epng::rgba_pixel elem) : northwest{nullptr}, northeast{nullptr}, southwest{nullptr}, southeast{nullptr}, element{elem}
		{
			// No implementation
		}

    };
	uint64_t res_;
    std::unique_ptr<node> root_; // the root of the tree
	//void print(std::ostream &out, const node * current, int level) const;
	//bool equal(const node * firstPtr, const node * secondPtr) const;
	void quadcopy(std::unique_ptr<node> &root1, std::unique_ptr<node> const &root2);
	void buildTree(const epng::png &source, uint64_t resolution, uint64_t x, uint64_t y, std::unique_ptr<node> &root);
	const epng::rgba_pixel & getPixel(uint64_t x, uint64_t y, uint64_t resolution, node * root) const;
	void rotate_clockwise(node * root);
	void prune(uint32_t tolerance, node * root);
	bool pruner(uint32_t tolerance, node * curr, node * root);
	uint64_t pruned_size(uint32_t tolerance, node * root) const;
	bool sizehelper(uint32_t tolerance, node * curr, node * root) const;
	bool has_children(node * root);
	bool has_children(node * root) const;
	uint32_t ideal_prune(uint64_t num_leaves, uint32_t min, uint32_t max) const;
	
/**** Do not remove this line or copy its contents here! ****/
#include "quadtree_given.h"
};
}
#endif
