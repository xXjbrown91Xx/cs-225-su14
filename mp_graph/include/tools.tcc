/**
 * @file tools.tcc
 * Definition of utility functions on graphs.
 */

#include <limits>
#include <queue>

#include "dsets.h"
#include "tools.h"

namespace cs225
{
namespace tools
{

template <class Graph>
typename Graph::edge_weight_type min_weight_edge(Graph& g)
{
    using weight_t = typename Graph::edge_weight_type;
    weight_t min = std::numeric_limits<weight_t>::max();
    edge min_edge{g.start_vertex(), g.start_vertex()};

	// Set all the edges and vertices to UNEXPLORED
	edge_map<edge_state> edge_mark;
    vertex_map<vertex_state> vtx_mark;
    for (const auto& v : g.vertices())
        vtx_mark[v] = vertex_state::UNEXPLORED;
	for (const auto& e : g.edges())
		edge_mark[e] = edge_state::UNEXPLORED;

	// Visit all our vertices 
	for(const auto& vert : g.vertices())
	{
		if(vtx_mark[vert] == vertex_state::UNEXPLORED)
		{
			min_search(g, vert, min, min_edge, vtx_mark, edge_mark);
		}
	}
	// Set the minimum edge weight to appropriate label
	edge_mark[min_edge] = edge_state::MIN;
	g.edge_label(min_edge, edge_state::MIN);
	// Return the minimum weight
	return min;	
}

template <class Graph>
void min_search(Graph& g, vertex start, typename Graph::edge_weight_type & low, edge & smallest, vertex_map<vertex_state> & vmap, edge_map<edge_state> & emap)
{
	// Create a queue and push start vertex 
	std::queue<vertex> q;
	q.push(start);
	// Mark vertex as visited
	vmap[start] = vertex_state::VISITED;

	// While there are still vertex's to explore
	while(!q.empty())
	{
		// We will check to see if the current vertex has a min edge weight smaller than curr min
		auto u = q.front();
		q.pop();
		for(const auto& v : g.adjacent(u))
		{
			// Create an edge representation with given vertices
			auto e1 = edge{u,v};
			// Check if this edge is less than our current min
			if(g.edge_weight(e1) < low)
			{
				low = g.edge_weight(e1);
				smallest = e1;
			}
			
			// Update vertice and edge states
			if(vmap[v] == vertex_state::UNEXPLORED)
			{
				vmap[v] = vertex_state::VISITED;
				emap[e1] = edge_state::DISCOVERY;
				g.edge_label(e1, edge_state::DISCOVERY);
				q.push(v);
			}
			else if(emap[e1] == edge_state::UNEXPLORED)
			{
				emap[e1] = edge_state::CROSS;
				g.edge_label(e1, edge_state::CROSS);
			}
		}
	}
}

template <class Graph>
uint64_t shortest_path_length(Graph& g, vertex start, vertex end)
{
	// Return value
	uint64_t path_len = 1;
	// Mark all edges and vertices as unmarked
	edge_map<edge_state> edge_mark;
    vertex_map<vertex_state> vtx_mark;
    for (const auto& v : g.vertices())
        vtx_mark[v] = vertex_state::UNEXPLORED;
	for (const auto& e : g.edges())
		edge_mark[e] = edge_state::UNEXPLORED;

	// Initialize a queue
	std::queue<vertex> q;
	// Initialize container to keep track of parent
	std::unordered_map<vertex, vertex> root;
	// Push the start on to our queue and mark it as visited
	auto init = start;
	bool found = false;
	q.push(start);
	vtx_mark[start] = vertex_state::VISITED;
	// while there are still vertices unexplored and we have not found the end vertex
	while(!q.empty() && !found)
	{
		// Visit current vertex and adjacent vertices
		init = q.front();
		q.pop();
		for(const auto& v : g.vertices())
		{
			auto e = edge{init, v};
			// Check if current vertex is end
			if(v == end)
			{
				found = true;
				break;
			}
			// If not, edge and vertex labels and add vertex to root map
			else if(vtx_mark[v] == vertex_state::UNEXPLORED)
			{
				vtx_mark[v] = vertex_state::VISITED;
				edge_mark[e] = edge_state::DISCOVERY;
				q.push(v);
				root[v] = init;
			}
			else if(edge_mark[e] == edge_state::UNEXPLORED)
			{
				edge_mark[e] = edge_state::CROSS;
			}
		}
	}

	// Work backwards to label the shortest path
	auto e1 = edge{init, end};
	edge_mark[e1] = edge_state::MINPATH;
	while(init != start)
	{
		auto e2 = edge{init, root[init]};
		edge_mark[e2] = edge_state::MINPATH;
		init = root[init];
		path_len++;
	}

	// Return the shortest path length
	return path_len;
}
			
template <class Graph>
void mark_mst(Graph& g)
{
    for (const auto& e : minimum_spanning_tree(g))
        g.edge_label(e, edge_state::MST);
}

template <class Graph>
edge_set minimum_spanning_tree(const Graph& g)
{
    // sort all the edges
    auto struts = g.vertices();
    auto edges = g.edges();
    std::vector<edge> pq{edges.begin(), edges.end()};
	//std::sort(pq.begin(), pq.end());
    std::sort(pq.begin(), pq.end(), [&](edge e1, edge e2)
     {return g.edge_weight(e1) < g.edge_weight(e2); });
    
	// Initialize an edge set for our output
	edge_set out_edges;
	// Initialize a queue for the edges
	std::queue<edge> q;
	for(uint64_t i = 0; i < pq.size(); i++)
	{
		q.push(pq[i]);
	}
	// Initialize a disjoint set so that every vertex is in its own set
	dsets s;
	s.add_elements(struts.size());
	// Until n-1 edges have been selected
	for(uint64_t i = 0; i < pq.size(); i++)
	{
		// Add the edge if no cycle is created, else ignore
		if(s.find(pq[i].source) != s.find(pq[i].dest))
		{
			// Push edge on to the output set
			out_edges.insert(pq[i]);
			// Merge the two vertices that own the edge
			s.merge(pq[i].source, pq[i].dest);
		}
	}

	return out_edges;
}
			
		
}
}
