#include "dsets.h"
#include <stdio.h>
#include <iostream>

using namespace std;

void dsets::add_elements(uint64_t num)
{
	for(uint64_t i = 0; i < num; i++)
	{
		arr_.emplace_back(-1);
	}
	
}

uint64_t dsets::find(uint64_t elem)
{
	if(arr_[elem] < 0)
	{
		return elem;
	}
	return arr_[elem] = find(arr_[elem]);
}

void dsets::merge(uint64_t a, uint64_t b)
{
	auto root1 = find(a);
	auto root2 = find(b);

	if(root1 == root2)
	{
		return;
	}
	
	if(arr_[root1] <= arr_[root2])
	{
		arr_[root1] += arr_[root2];
		arr_[root2] = root1;
	}
	else
	{
		arr_[root2] += arr_[root1];
		arr_[root1] = root2;
	}
}
