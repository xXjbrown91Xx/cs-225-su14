/**
 * @file scoped_ptr.tcc
 * @author Chase Geigle
 * @date Summer 2014
 */

#include "scoped_ptr.h"
#include <utility>
#include <algorithm>

namespace cs225
{

template <class T>
scoped_ptr<T>::scoped_ptr()
{
	ptr_ = nullptr;
}

template <class T>
scoped_ptr<T>::scoped_ptr(T* rawptr)
{
	ptr_ = rawptr;
}

template <class T>
scoped_ptr<T>::scoped_ptr(scoped_ptr&& other)
{
	/* set our pointer to NULL */
	ptr_ = nullptr;
	/* Our pointer get contents of other pointer, other is left empty */
	swap(other);
}

template <class T>
scoped_ptr<T>& scoped_ptr<T>::operator=(scoped_ptr&& other)
{
	if(this != &other)
	{
		/* Release any resources that *this currently owns */
		clear();
		/* Take the other objects resources and leave it empty */
		ptr_ = nullptr;
		swap(other);
		/* Return */
		return *this;
	}
	return *this;
}

template <class T>
scoped_ptr<T>::~scoped_ptr()
{
	clear();
}

template <class T>
void scoped_ptr<T>::clear()
{
	delete ptr_;
	ptr_ = nullptr;
}

template <class T>
void scoped_ptr<T>::swap(scoped_ptr<T>& other)
{
	std::swap(ptr_,other.ptr_);
}

template <class T>
bool scoped_ptr<T>::empty() const
{
	if(ptr_ != nullptr)
	{
		return false;
	}
	return true;
}

template <class T>
const T& scoped_ptr<T>::operator*() const
{
	/* Check for NULL dereference */
	if(ptr_ != nullptr)
	{
		return *ptr_;
	}
	throw;
}

template <class T>
T& scoped_ptr<T>::operator*()
{
	/* Check for NULL dereference */
	if(ptr_ != nullptr)
	{
		return *ptr_;
	}
	throw;
}

template <class T>
const T* scoped_ptr<T>::operator->() const
{
	/* Check for NULL dereference */
	if(ptr_ != nullptr)
	{
		return ptr_;
	}
	return nullptr;
}

template <class T>
T* scoped_ptr<T>::operator->()
{
	/* Check for NULL dereference */
	if(ptr_ != nullptr)
	{
		return ptr_;
	}
	return nullptr;
}

template <class T>
const T* scoped_ptr<T>::get() const
{
	return ptr_;
}

template <class T>
T* scoped_ptr<T>::get()
{
	return ptr_;
}
}

