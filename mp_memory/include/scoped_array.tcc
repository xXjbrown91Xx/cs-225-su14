/**
 * @file scoped_array.tcc
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "scoped_array.h"
#include <utility>
#include <algorithm>
#include <exception>
#include <iostream>
#include <vector>

namespace cs225
{
template<class T>
scoped_array<T>::scoped_array()
{
	arr_ = nullptr;
	size_ = 0;
}

template<class T>
scoped_array<T>::scoped_array(uint64_t sze)
{
	arr_ = new T[sze];
	size_ = sze;
}

template<class T>
scoped_array<T>::scoped_array(const scoped_array<T> & other)
{
	/* Copy over the size */
	size_ = other.size_;
	/* Create new array of others size, and fill in the copy */
	arr_ = new T[size_];
	for(uint64_t i = 0; i<size_; i++)
	{
		arr_[i] = other[i];
	}
}

template<class T>
scoped_array<T>::scoped_array(scoped_array<T> && other)
{
	/* Set the size of arr_ */
	size_ = 0;
	/* arr_ will now point to NULL */
	arr_ = nullptr;
	/* swap points arr_ to where other currently points and points other to NULL */
	swap(other);
}

template<class T>
scoped_array<T>& scoped_array<T>::operator=(scoped_array<T> rhs)
{
	if(this != &rhs)
	{
		/* Release any resources that *this currently owns */
		clear();
		/* Take the other objects resources, leaving it empty */
		swap(rhs);
		/* Return */
		return *this;
	}
	return *this;
}

template<class T>
scoped_array<T>::~scoped_array()
{
	clear();
}

template<class T>
void scoped_array<T>::clear()
{
	delete [] arr_;
	arr_ = nullptr;
	size_ = 0;
}

template<class T>
void scoped_array<T>::swap(scoped_array<T> &other)
{
	std::swap(arr_, other.arr_);
	std::swap(size_, other.size_);
}

template<class T>
void scoped_array<T>::resize(uint64_t size)
{
	/* Creating temporary storage array for move */
	T * temp_arr = new T[size];
	/* Case : size_ < size */
	if(size_ < size)
	{   
		/* Copy over elements of arr_ into temp array */
		for(uint64_t i = 0; i<size_; i++)
		{
			temp_arr[i] = arr_[i];
		}
		/* Fill the remaining elements of temp array with zero */
		for(uint64_t i = size_; i<size; i++)
		{
			temp_arr[i] = 0;
		}
	}
	/* Case : size_ > size */
	if(size_ > size)
	{
		/* Copy over elements of arr_ into temp array */
		for(uint64_t i = 0; i<size; i++)
		{
			temp_arr[i] = arr_[i];
		}
		/* Delete the remaining elements of old array */
		for(uint64_t i = size_; i<size; i++)
		{
			arr_[i] = -1;
		}
	}

	/* Update member variables */
	size_ = size;
	delete [] arr_;
	arr_ = temp_arr;
} 

template<class T>
const T& scoped_array<T>::operator[](uint64_t idx) const
{
	/* Check for Null dereference */
	if(arr_ != nullptr)
	{
		return arr_[idx];
	}
	throw;
}

template<class T>
T& scoped_array<T>::operator[](uint64_t idx)
{
	/* Check for null dereference */
	if(arr_ != nullptr)
	{
		return arr_[idx];
	}
	throw;
}

template<class T>
const T& scoped_array<T>::at(uint64_t idx) const
{
	if(idx >= size_)
	{
		throw std::out_of_range{":invalid index"};
	}
	return arr_[idx];
}

template<class T>
T& scoped_array<T>::at(uint64_t idx)
{
	if(idx >= size_)
	{
		throw std::out_of_range{":invalid index"};
	}
	return arr_[idx];
}

template<class T>
uint64_t scoped_array<T>::size() const
{
	return size_;
}

template<class T>
bool scoped_array<T>::empty() const
{
	if(size_ == 0 || arr_ == nullptr)
	{
		return true;
	}
	return false;
}
}
