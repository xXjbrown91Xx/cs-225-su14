/**
 * @file collage.cpp
 * @author Jorden Brown
 * @date Summer 2014
 */

#include "collage.h"
#include "epng.h"
#include <utility>
#include <iostream>
#include <exception>
#include <algorithm>

namespace cs225
{
collage::collage(uint64_t size) : img(std::move(new scoped_array<scoped_ptr<epng::png>>[size]()))
{
	//size_ = size;
	//x_pos = new uint64_t[size_];
	//y_pos = new uint64_t[size_];
	//for(uint64_t i = 0; i<size_; i++)
	//{
	//	x_pos[i] = 0;
	//	y_pos[i] = 0;
	//}
}
collage::collage(const collage &other) : img(std::move(new scoped_array<scoped_ptr<epng::png>>[other.size_]))
{
	/* Copy over the size */
	//size_ = other.size_;
	/* Create independent variables of necessary data members */
	//x_pos = new uint64_t[size_];
	//y_pos = new uint64_t[size_];
	/* Iterate over the entire image and copy image */
	//for(uint64_t i = 0; i<size_; i++)
	//{
	//	img[i] = other.img[i];
	//	x_pos[i] = other.x_pos[i];
	//	y_pos[i] = other.y_pos[i];
	//}
}
collage::collage(collage &&other)
{
	/* Strip current instance of its resources */
	//size_ = 0;
	//img = nullptr;
	//x_pos = nullptr;
	//y_pos = nullptr;
	/* Swap with other instance */
	//swap(other);
}
collage& collage::operator=(collage rhs)
{
	//if(this != &rhs)
	//{
		/* Release the resources of the current instance */
	//	clear();
		/* Take the other objects resources, leaving it empty */
	//	swap(rhs);
		/* Return */
	//	return *this;
	//}
	//return *this;
}
collage::~collage()
{
	//clear();
}
void collage::clear()
{
	//delete [] y_pos;
	//delete [] x_pos;
	//for(uint64_t i = 0; i<size_; i++)
	//{
	//	delete img[i].ptr_;
	//}
	//delete [] img; // Fix this 
}
void collage::swap(collage &other)
{
	//std::swap(size_, other.size_);
	//std::swap(img, other.img);
	//std::swap(x_pos, other.x_pos);
	//std::swap(y_pos, other.y_pos);
}
void collage::layers(uint64_t max)
{
	/* Creating temporary storage for dynamic variables */
	//scoped_array<scoped_ptr<epng::png>> * img_temp = new scoped_array<scoped_ptr<epng::png>>[max];
	//uint64_t * temp_x = new uint64_t[max];
	//uint64_t * temp_y = new uint64_t[max];
	/* Case: size_ < max */
	//if(size_ < max)
	//{
		/* Copy over all the elements of each array */
		//for(uint64_t i = 0; i<size_; i++)
		//{
		//	img_temp[i] = img[i];
		//	temp_x[i] = x_pos[i];
		//	temp_y[i] = y_pos[i];
		//}
		/* Set extra value of temp arrays to a default value */
		//for(uint64_t i = size_; i<max; i++)
		//{
		//	img_temp[i] = 0; 
		//	temp_x[i] = 0;
		//	temp_y[i] = 0;
		//}
	//}
	/* Case size_ > max */
	//if(size_ > max)
	//{
	//	/* Copy over all the elements of each array */
	//	for(uint64_t i = 0; i<max; i++)
	//	{
	//		img_temp[i] = img[i];
	//		temp_x[i] = x_pos[i];
	//		temp_y[i] = y_pos[i];
	//	}
		/* Remove remaining elements */
	//	for(uint64_t i = max; i<size_; i++)
	//	{
			//delete img_temp[i];
	//		img_temp[i] = 0;  // May need to change
	//		temp_x[i] = -1;
	//		temp_y[i] = -1;
	//	}
	//}
	/* Update member variables */
	//size_ = max;
	//clear();
	//img = img_temp;
	//x_pos = temp_x;
	//y_pos = temp_y;
}
uint64_t collage::layers() const
{
	//return size_ - 1;
	return 0;
}
uint64_t collage::filled() const
{
	/* To implement */
	return 0;
}
void collage::emplace_picture(const std::string & filename, uint64_t index, uint64_t x, uint64_t y)
{
	/* Index out of range check */
//	if(index >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	/* index occupied check */
//	if(img[index]!= nullptr)
//	{
//		delete img[index];
//	}
//	/* Creating new image */
//	epng::png * tile = new epng::png(size_, size_);
//	/* Loading in given image */
//	tile->load(filename);
//	/* updating member variables */
//	img[index] = new epng::png(*tile);
//	x_pos[index] = x;
//	y_pos[index] = y;
}
void collage::change_layer(uint64_t src, uint64_t dest)
{
	/* Index out of range check */
//	if(src >= size_ || dest >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	/* Index occupied check */
//	if(img[dest] != nullptr)
//	{
//		delete img[dest];
//	}
//	img[dest] = img[src];
//	x_pos[dest] = x_pos[src];
//	y_pos[dest] = y_pos[src];
//	delete img[src];
//	img[src] = nullptr;
//	x_pos[dest] = 0;
//	y_pos[dest] = 0;
}
void collage::position(uint64_t index, uint64_t x, uint64_t y)
{
//	/* Index out of range check */
//	if(index >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	/* Image empty check */
//	if(img[index] == nullptr)
//	{
//		throw std::invalid_argument{":No Image"};
//	}
//	/* Set the position */
//	x_pos[index] = x;  // May have to use this here
//	y_pos[index] = y;
}
void collage::erase(uint64_t index)
{
//	/* Index out of range check */
//	if(index >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	delete img[index];
//	img[index] = nullptr;
//	x_pos[index] = 0;
//	y_pos[index] = 0;
}
const epng::png * collage::at(uint64_t index) const
{
//	/* Index out of range check */
//	if(index >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	return img[index];
	epng::png * thing = new epng::png();
	return thing;
}
epng::png * collage::at(uint64_t index)
{
//	/* Index out of range check */
//	if(index >= size_)
//	{
//		throw std::out_of_range{":invalid index"};
//	}
//	return img[index];
	epng::png * thing = new epng::png();
	return thing;
}

epng::png collage::draw() const
{
	/* Implement me */
	return epng::png();
}
}

