/**
 * @file heap.cpp
 * Implementation of a heap class.
 */

#include<math.h>
#include<algorithm>
#include<utility>

template <class T, class Compare>
size_t heap<T, Compare>::root() const
{
	return 0;
}

template <class T, class Compare>
size_t heap<T, Compare>::left_child(size_t idx) const
{
    return (2*idx) + 1;
}

template <class T, class Compare>
size_t heap<T, Compare>::right_child(size_t idx) const
{
	return (2*idx) + 2;
}

template <class T, class Compare>
size_t heap<T, Compare>::parent(size_t idx) const
{
	return floor((idx-1)/2);
}

template <class T, class Compare>
bool heap<T, Compare>::has_child(size_t idx) const
{
	if(left_child(idx) > elems_.size()-1)
	{
		return false;
	}
	return true;
}

template <class T, class Compare>
size_t heap<T, Compare>::max_priority_child(size_t idx) const
{
    if(has_child(idx))
	{
		if(right_child(idx)>(elems_.size()-1))
		{
			return left_child(idx);
		}
		if(higher_priority_(elems_[left_child(idx)], elems_[right_child(idx)]))
		{
			return left_child(idx);
		}
		return right_child(idx);
	}
	return -1;
}

template <class T, class Compare>
void heap<T, Compare>::heapify_down(size_t idx)
{
	if(has_child(idx))
	{
		int val = max_priority_child(idx);
		if(!higher_priority_(elems_[idx], elems_[val]))
		{
			std::swap(elems_[idx], elems_[val]);
			heapify_down(left_child(idx));
			heapify_down(right_child(idx));
		}
	}
}

template <class T, class Compare>
void heap<T, Compare>::heapify_up(size_t idx)
{
    if (idx == root())
        return;
    size_t parentIdx = parent(idx);
    if (higher_priority_(elems_[idx], elems_[parentIdx]))
    {
        std::swap(elems_[idx], elems_[parentIdx]);
        heapify_up(parentIdx);
    }
}

template <class T, class Compare>
heap<T, Compare>::heap()
{
	//elems_.push_back(-1);
}

template <class T, class Compare>
heap<T, Compare>::heap(const std::vector<T>& elems)
{
	elems_ = elems;
	for(int i = parent(elems_.size() -1); i>= 0; i--)
	{
		heapify_down(i);
	}
}

template <class T, class Compare>
void heap<T, Compare>::pop()
{
    std::swap(elems_[0], elems_[elems_.size()-1]);
	elems_.pop_back();
	if(elems_.size() == 0)
	{
		return;
	}
	heapify_down(0);
}

template <class T, class Compare>
const T& heap<T, Compare>::peek() const
{
    return elems_[0];
}

template <class T, class Compare>
void heap<T, Compare>::push(T elem)
{
	elems_.push_back(elem);
	heapify_up(elems_.size()-1);
}

template <class T, class Compare>
bool heap<T, Compare>::empty() const
{
    if(elems_.size() == 0)
	{
		return true;
	}
	return false;
}
